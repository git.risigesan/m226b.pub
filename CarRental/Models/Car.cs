﻿using System;

namespace CarRental.Models
{
    public abstract class Car : Vehicle
    {
        public int TrunkVolume { get; private set; }

        public Car(string model, string brand, int seats, float price, float parkingSpace, int horsePower, bool ac, int trunkVolume)
            : base(model, brand, seats, price, parkingSpace, horsePower, ac) => TrunkVolume = trunkVolume;

        public override void PrintInfos()
        {
            Console.WriteLine("TrunkVolume: " + this.TrunkVolume + "\n");
            base.PrintInfos();
        }
    }
}
