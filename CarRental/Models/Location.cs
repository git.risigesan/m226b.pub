﻿namespace CarRental.Models
{
    public class Location
    {
        public string Street;
        public string HouseNumber;
        public string PostalCode;
        public string City;
        public string Country;

        public Location(string street, string houseNumber, string postaleCode, string city, string country)
        {
            Street = street;
            HouseNumber = houseNumber;
            PostalCode = postaleCode;
            City = city;
            Country = country;
        }
    }
}
