﻿using System;

namespace CarRental.Models
{
    public class Employee : Person
    {
        public string Job;
        public bool Status;

        public Employee(string firstName, string lastName, string email, string phonenumber, string job, bool status)
            : base(firstName, lastName, email, phonenumber) => (Job, Status) = (job, status);
        public void printData()
        {

            Console.WriteLine("Name:" + FullName + "\n" +
                    "PhoneNumber:" + Phonenumber + "\n" +
                    "Email:" + Email + "\n" +
                    "Job:" + Job);
        }
    }
}
