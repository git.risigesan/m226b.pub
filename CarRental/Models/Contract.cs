﻿using System;

namespace CarRental.Models
{
    public abstract class Contract
    {
        private Client Client;
        private int ContractId;
        private Vehicle CarNumber;
        private float Price;
        private Employee Employee;
        private Equipment Equipment;
        private string TillDate;
        private string FromDate;

        public Contract()
        : base() => (Client, ContractId, CarNumber, Price, Employee, Equipment, TillDate, FromDate) = (Client, ContractId, CarNumber, Price, Employee, Equipment, TillDate, FromDate);
        public void PrintInfo()
        {
            Console.WriteLine("Vehiclenumber: " + CarNumber +
                $"\nPrice: " + Price + "Extra Equipment: " + Equipment +
                $"\nFromdate-Tilldate: " + FromDate + "-" + TillDate);
        }
    }
}
