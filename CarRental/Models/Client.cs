﻿namespace CarRental.Models
{
    public class Client : Person
    {
        public int ClientNumber;
        public string Iban;
        public Contract contractNumber { get; set; }

        public void rent()
        {

        }

        public Client(string firstName, string lastName, string email, string phonenumber , string iban, int clientnumber)
            : base(firstName, lastName, email, phonenumber) => (Iban, ClientNumber) = (iban ,clientnumber);
    }
}