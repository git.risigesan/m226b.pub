﻿using System;

namespace CarRental.Models
{
    public abstract class Vehicle
    {
        public string Model { get; set; }
        public string Brand { get; set; }
        public int Seats { get; set; }
        public float Price { get; set; }
        public float ParkingSpace { get; set; }


        // Additionally
        public int HorsePower { get; set; }
        public bool AC { get; set; }

        public Vehicle(string model, string brand, int seats, float price, float parkingSpace, int horsePower, bool ac)
        {
            Model = model;
            Brand = brand;
            Seats = seats;
            Price = price;
            ParkingSpace = parkingSpace;

            // Additionally
            HorsePower = horsePower;
            AC = ac;
        }

        virtual void PrintInfos()
        {
            throw new NotImplementedException();
        }
    }
}
