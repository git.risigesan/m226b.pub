﻿using System;

namespace CarRental.Models
{
    public abstract class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phonenumber { get; set; }
        public string Email { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public Person(string firstName, string lastName, string email, string phonenumber)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Phonenumber = phonenumber;
        }

        public virtual void PrintInfo()
        {
            Console.WriteLine(  "Name:" + FullName + "\n" +
                                "PhoneNumber:" + Phonenumber + "\n" +
                                "Email:" + Email);
        }
    }
}
