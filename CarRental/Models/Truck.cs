﻿namespace CarRental.Models
{
    public class Truck : Vehicle
    {
        public int Height { get; set; }
        public int Width { get; set; }

        public Truck(string model, string brand, int seats, float price, float parkingSpace, int horsePower, bool ac, int height, int width)
            : base(model, brand, seats, price, parkingSpace, horsePower, ac) => (Height, Width) = (height, width);

    }
}
