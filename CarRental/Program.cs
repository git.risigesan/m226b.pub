﻿using CarRental.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace CarRental
{
    class Program
    {
        static void Main(string[] args)
        {
            #region filestream
            //    List<Vehicle> vehicles = new List<Vehicle>();

            //    Car alhambra = new Car("ZG-345 443", false, "Seat Alhambra");
            //    Truck actros = new Truck("ZG-543 133", true, "Mercedes Actros");

            //    vehicles.Add(alhambra);
            //    vehicles.Add(actros);

            //    //Binary file

            //    FileStream fs = new FileStream(fileNameBin, FileMode.Create);
            //    IFormatter bf = new BinaryFormatter();

            //    bf.Serialize(fs, vehicles);

            //    fs.Position = 0;
            //    Console.WriteLine("\n\nReconstructed Clients:\n");

            //    // Reads a list of objects from a binary file
            //    List<Vehicle> recList = (List<Vehicle>)bf.Deserialize(fs);
            //    foreach (Vehicle count in recList)
            //    {
            //        count.printClient();
            //        Console.WriteLine();
            //    }
            //    fs.Close();

            //    //Txt File

            //    //create StreamWriter-object
            //    StreamWriter sw = new StreamWriter(fileNameTxt);

            //    sw.WriteLine($"Name: {alhambra.name}");
            //    sw.WriteLine($"Number Plate: {alhambra.numberPlate}");
            //    sw.WriteLine($"Is Rented: {alhambra.isRented}");

            //    sw.WriteLine();

            //    sw.WriteLine($"Name: {actros.name}");
            //    sw.WriteLine($"Number Plate: {actros.numberPlate}");
            //    sw.WriteLine($"Is Rented: {actros.isRented}");
            //    sw.Close();
            #endregion
            bool stayInShop = true;
            
            List<Client> client = new List<Client>
            {
                new Client("Johnny", "Deep", "johnny.deep@mail.com", "0041411234567", "0000000000000000AAA", 1),
                new Client("Vijay", "Devarakonda", "vijay.devarakonda@mail.com", "0041411234567", "0000000000000000AAA", 2)
            };

            List<Employee> employees = new List<Employee>
            {
                new Employee("Bausa", "Bert", "bausa.bert@risiworks.com", "0041411234567", "Kauffrau", false),
                new Employee("Vijay", "Devarakonda", "vijay.devarakonda@mail.com", "0041411234567", "Kauffrau", false)
            };

            //List<Contract> contracts = new List<Contract>
            //{
            //    new Contract();
            //}

            Console.WriteLine("Willkommen in der Autovermietung [risiworks]\n");
            while (stayInShop)
            {
                Console.WriteLine("Wollen Sie ein Fahrzeug meiten? (Y/N)");
                string answer = Console.ReadLine();

                if (answer == "Y" || answer == "y")
                {
                    Console.WriteLine("Herzlich willkommen, Sie können sich einen Mitarbeiter aussuchen:");
                    while (stayInShop)
                    {
                        for (int i = 0; i < employees.Count; i++)
                        {
                            if(employees[i].Job == "Kauffrau")
                            {
                                Console.WriteLine("\n" + i + "|");
                                employees[i].printData();
                            }
                        }
                        answer = Console.ReadLine();
                        if (Int32.Parse(answer) <= employees.Count)
                        {
                            if(employees[Int32.Parse(answer)].Status == false)
                            {
                                employees[Int32.Parse(answer)].Status = true;
                                Console.WriteLine(employees[Int32.Parse(answer)].FirstName + " " + employees[Int32.Parse(answer)].LastName + " heisst Sie recht Herzlich Willkommen in der risiworks Autovermietung");
                                
                            }
                            else
                            {
                                Console.WriteLine("Der Ausgewählte Mitarbeiter ist leider zur Zeit besetzt, wählen Sie sich einen anderen Mitarbeiter aus oder warten Sie bis der Mitarbeier frei wird!");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Ihr Ausgewählter Mitarbeiter existiert nicht!\nVersuchen Sie es nochmals, wählen Sie einen der unter aufgelisteten Mitarbeiter aus:");
                        }
                    }
                }
                else if (answer == "N" || answer == "n")
                {
                    Console.WriteLine("Ok bye");
                    stayInShop = false;
                }
                else
                {
                    Console.WriteLine("You canceled the conversation and stayed in the shop.");
                }

            }

        }
    }
}
