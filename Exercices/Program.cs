﻿using System;
using System.IO;

namespace Exercices
{
    class Program
    {
        static void Main(string[] args)
        {
            // define and initialize arrays
            byte[] byteArrayWrite = { 200, 201, 202, 203, 204, 205, 206, 207 }; 
            byte[] byteArrayRead = new byte[byteArrayWrite.Length];

            //create file stream
            FileStream fs = new FileStream(@"H:\GIT\M226B\m226b\variable.txt", FileMode.Create);

            // write array to file 
            fs.Write(byteArrayWrite, 0, byteArrayWrite.Length);

            // output: values of byte array
            for (int count = 0; count < byteArrayRead.Length; count++){
                Console.Write (byteArrayRead[count] + ", ");       
            } 

            // close filestream      
            fs.Close();

        }
    }
}
